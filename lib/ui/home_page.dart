import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;


//**************************** CLASS HOME PAGE *********************************
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

//****************************** CLASS STATE (HOME) ****************************
class _HomePageState extends State<HomePage> {

  //VARIABLE
  String _search;
  int _offset = 0;

  //***************************** INIT STATE ******************************
  @override
  void initState() {
     super.initState();

     _getGifs().then((map){

       print(map);
     });
  }

  //**************************** SEARCH GIFTS *****************************
  Future<Map> _getGifs() async{

    http.Response response;

    if(_search == null)
      response = await http.get("https://api.giphy.com/v1/gifs/trending?api_key=aENQXq9F9qZCC34SLUyK3ZeHo2ZyhAkg&limit=20&rating=G");
    else
      response = await http.get("https://api.giphy.com/v1/gifs/search?api_key=aENQXq9F9qZCC34SLUyK3ZeHo2ZyhAkg&q=$_search&limit=25&offset=$_offset&rating=G&lang=pt");

    return json.decode(response.body);
  }


  //**************************** MAIN WIDGET *******************************
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
//******************************************************************************
